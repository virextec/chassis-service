# chassis-service

template to build web-service

# Resources

- java 11
- maven >= 3.5.x
- docker
- docker-compose >=3.1
- make (optional)

# build

```
make down fast-install up
```